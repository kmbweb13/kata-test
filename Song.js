var Frame = (function () {
	var Clazz = function () {
		this.strike = false;
		this.spare = false;
		this.roll = [];
		this.score = 0;
	}

	Clazz.prototype.init = function (rolls) {
		this.roll = rolls;
		var total = 0;
		for (var i = 0, len = rolls.length; i < len; i++) {
			total += rolls[i];
			if (total === 10) {
				if(i === 0) {
					this.strike = true;
					if ( len === 3) {
						this.score = total + rolls[1] + rolls[2];
					} 
					break;
				} 
				else if (i === 1) {
					this.spare = true;
				}
			}
		}
	}

	Clazz.prototype.calcScore = function (frames) {
		var rollScore = this.roll[0] + this.roll[1], len = frames.length;
		if (this.roll[2]) {
			rollScore += this.roll[2];
			this.score = rollScore;
		}
		else {
			if (this.strike ) {
			
				if (len > 0) {
					var nextFrame = frames[0];
					rollScore += nextFrame.roll[0];
					rollScore += nextFrame.roll[1];

					if(nextFrame.strike && len == 2) {
						rollScore += frames[1].roll[0];
					}
				}

				this.score = rollScore;
			} 
			else if (this.spare) {
				this.score = rollScore += frames[0].roll[0];;
			}
			else {
				this.score = rollScore;
			} 
		}

	}

	return Clazz;	
})();

var Bowling  = (function() {
	var Clazz = function () {
		this.compete = false;
		this.finalScore = 0;

	}

	Clazz.prototype.processGame = function(frames) {
		var gameFrames = [], len = frames.length, i = 0; 
		for (; i < len; i++) {
			var frame = new Frame();
			frame.init(frames[i]);
			gameFrames.push(frame);
		}

		for (i = 0; i < len; i++) {
			var currentFrame = gameFrames[i];
			var nextFrames = [];
			for (var j = 1; j < 3 ; j++) {
				var index = i + j;
				if (index < len) {
					nextFrames.push(gameFrames[index]);
				}
			}
			currentFrame.calcScore(nextFrames);

			this.finalScore += currentFrame.score;
		}
	}
	return Clazz;
})();

