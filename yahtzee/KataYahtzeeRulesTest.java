package com.kmbweb.kataYahtzee;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class KataYahtzeeRulesTest {
	private KataYahtzeeRules ky;
	@Before
	public void setUp() throws Exception {
		ky = new KataYahtzeeRules();
	}

	@Test
	public void processAllOnes() {
		assertEquals(ky.processAllOnes(new int[] {1,1,1,1,2}), 4);
		assertEquals(ky.processAllOnes(new int[] {1,1,2,2,2}), 2);
		assertEquals(ky.processAllOnes(new int[] {1,1,1,1,1}), 5);
		assertEquals(ky.processAllOnes(new int[] {6,6,5,2,1}), 1);
	}
	@Test
	public void processAllTwos() {
		assertEquals(ky.processAllTwos(new int[] {1,1,1,1,2}), 2);
		assertEquals(ky.processAllTwos(new int[] {1,2,2,2,2}), 8);
		assertEquals(ky.processAllTwos(new int[] {2,2,2,2,2}), 10);
		assertEquals(ky.processAllTwos(new int[] {2,3,5,1,2}), 4);
	}
	@Test
	public void processAllThrees() {
		assertEquals(ky.processAllThrees(new int[] {1,1,1,2,3}), 3);
		assertEquals(ky.processAllThrees(new int[] {1,1,2,3,3}), 6);
		assertEquals(ky.processAllThrees(new int[] {3,3,3,3,3}), 15);
		assertEquals(ky.processAllThrees(new int[] {3,1,6,2,3}), 6);
	}
	@Test
	public void processAllFours() {
		assertEquals(ky.processAllFours(new int[] {1,3,4,5,5}), 4);
		assertEquals(ky.processAllFours(new int[] {1,4,4,4,5}), 12);
		assertEquals(ky.processAllFours(new int[] {4,4,4,4,4}), 20);
		assertEquals(ky.processAllFours(new int[] {6,3,2,4,4}), 8);
	}
	@Test
	public void processAllFives() {
		assertEquals(ky.processAllFives(new int[] {1,1,2,3,4}), 0);
		assertEquals(ky.processAllFives(new int[] {1,5,5,5,6}), 15);
		assertEquals(ky.processAllFives(new int[] {5,5,5,5,5}), 25);
		assertEquals(ky.processAllFives(new int[] {6,6,6,5,5}), 10);
	}
	@Test
	public void processAllSixes() {
		assertEquals(ky.processAllSixes(new int[] {1,1,5,5,5}), 0);
		assertEquals(ky.processAllSixes(new int[] {4,5,6,6,6}), 18);
		assertEquals(ky.processAllSixes(new int[] {6,6,6,6,6}), 30);
		assertEquals(ky.processAllSixes(new int[] {6,5,2,2,6}), 12);
	}
	@Test
	public void processPair() {
		assertEquals(ky.processPair(new int[] {1,2,3,4,5}), 0);
		assertEquals(ky.processPair(new int[] {1,4,2,3,4}), 8);
		assertEquals(ky.processPair(new int[] {5,3,6,3,5}), 10);
		assertEquals(ky.processPair(new int[] {4,6,4,6,5}), 12);
	}
	@Test
	public void processTwoPair() {
		assertEquals(ky.processTwoPair(new int[] {1,2,3,4,5}), 0);
		assertEquals(ky.processTwoPair(new int[] {1,4,2,2,4}), 12);
		assertEquals(ky.processTwoPair(new int[] {5,3,6,3,5}), 16);
		assertEquals(ky.processTwoPair(new int[] {4,6,4,6,6}), 20);
	}
	
	@Test
	public void processThreeOfAKind() {
		assertEquals(ky.processThreeOfAKind(new int[] {1,2,3,4,5}), 0);
		assertEquals(ky.processThreeOfAKind(new int[] {3,2,3,4,3}), 9);
		assertEquals(ky.processThreeOfAKind(new int[] {1,4,2,2,4}), 0);
	}

	@Test
	public void processFourOfAKind() {
		assertEquals(ky.processFourOfAKind(new int[] {1,2,3,4,5}), 0);
		assertEquals(ky.processFourOfAKind(new int[] {3,2,3,3,3}), 12);
		assertEquals(ky.processFourOfAKind(new int[] {1,4,2,2,4}), 0);
	}
	
	@Test
	public void processSmallStraight() {
		assertEquals(ky.processSmallStraight(new int[] {1,2,3,4,5}), 15);
		assertEquals(ky.processSmallStraight(new int[] {5,2,4,3,1}), 15);
		assertEquals(ky.processSmallStraight(new int[] {1,4,2,2,4}), 0);
	}
	
	@Test
	public void processLargeStraight() {
		assertEquals(ky.processLargeStraight(new int[] {1,2,3,4,5}), 0);
		assertEquals(ky.processLargeStraight(new int[] {5,2,4,3,1}), 0);
		assertEquals(ky.processLargeStraight(new int[] {6,4,5,3,4}), 0);
		assertEquals(ky.processLargeStraight(new int[] {2,3,4,5,6}), 20);
	}
	
	@Test
	public void processFullHouse() {
		assertEquals(ky.processFullHouse(new int[] {1,2,3,4,5}), 0);
		assertEquals(ky.processFullHouse(new int[] {5,5,4,4,3}), 0);
		assertEquals(ky.processFullHouse(new int[] {1,1,1,3,3}), 9);
		assertEquals(ky.processFullHouse(new int[] {6,6,5,5,5}), 27);
	}

	@Test
	public void processYahtzee() {
		assertEquals(ky.processYahtzee(new int[] {1,2,3,4,5}), 0);
		assertEquals(ky.processYahtzee(new int[] {5,5,5,5,5}), 25);
		assertEquals(ky.processYahtzee(new int[] {1,1,1,3,3}), 0);
		assertEquals(ky.processYahtzee(new int[] {2,2,2,2,2}), 10);
	}
	@Test
	public void processChance() {
		assertEquals(ky.processChance(new int[] {1,2,3,4,5}), 15);
		assertEquals(ky.processChance(new int[] {5,6,6,4,5}), 26);
		assertEquals(ky.processChance(new int[] {1,1,1,3,3}), 9);
		assertEquals(ky.processChance(new int[] {6,6,5,2,2}), 21);
	}
}
