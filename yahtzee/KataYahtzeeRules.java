package com.kmbweb.kataYahtzee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class KataYahtzeeRules {
	// Assuming UI only allows int[] length = 5 and values of 1-6 with duplicates.
	private int processAllLikeNumbers(int[] is, int number) {
		int [] i = Arrays.stream(is).filter(x -> x == number).toArray();
		return i.length * number;
	}

	public int processAllOnes(int[] is) {
		return processAllLikeNumbers(is, 1);
	}

	public int processAllTwos(int[] is) {
		return processAllLikeNumbers(is, 2);
	}

	public int processAllThrees(int[] is) {
		return processAllLikeNumbers(is, 3);
	}

	public int processAllFours(int[] is) {
		return processAllLikeNumbers(is, 4);
	}

	public int processAllFives(int[] is) {
		return processAllLikeNumbers(is, 5);
	}
	
	public int processAllSixes(int[] is) {
		return processAllLikeNumbers(is, 6);
	}

	public int processPair(int[] is) {
		return processDuplicates(is, 2);
	}
	
	public int processTwoPair(int[] is) {
		int largePair = 0;
		List<Integer> list = convertToList(is);
		List<Integer> noDups = listWithNoDup(list);
		if (noDups.size() != list.size()){
			for(Integer i : noDups) {
				if(Collections.frequency(list, i) > 1) {
					if(largePair == 0) {
						largePair = i * 2;
					}
					else {
						return  largePair + i * 2;
					}					
				}
			}
		}		
		return 0;
	}

	public int processThreeOfAKind(int[] is) {
		return processDuplicates(is, 3);
	}

	public int processFourOfAKind(int[] is) {
		return processDuplicates(is, 4);
	}

	public int processSmallStraight(int[] is) {
		List<Integer> noDups = listWithNoDup(convertToList(is));
		if(noDups.size() == 5 && Collections.frequency(noDups, 6) == 0 ){
			return 15;
		}
		return 0;
	}

	public int processLargeStraight(int[] is) {
		List<Integer> noDups = listWithNoDup(convertToList(is));
		if(noDups.size() == 5 && Collections.frequency(noDups, 1) == 0 ){
			return 20;
		}
		return 0;
	}

	public int processFullHouse(int[] is) {
		int largeSet = 0, smallSet = 0;
		List<Integer> list = convertToList(is);
		List<Integer> noDups = listWithNoDup(list);
		if (noDups.size() == 2){
			for(Integer i : noDups) {
				if(Collections.frequency(list, i) == 3) {
					largeSet = i * 3;
				}
				else {
					smallSet = i * 2;					
				}
				if(largeSet != 0 && smallSet != 0) {
					return largeSet + smallSet;
				}
			}
		}		
		return 0;
	}
	

	public int processYahtzee(int[] is) {
		List<Integer> noDups = listWithNoDup(convertToList(is));
		if(noDups.size() == 1){
			return noDups.get(0) * 5;
		}
		return 0;
	}
	
	public int processChance(int[] is) {
		return Arrays.stream(is).sum();
	}

	private int processDuplicates(int[] is, int mincount) {
		List<Integer> list = convertToList(is);
		List<Integer> noDups = listWithNoDup(list);
		if (noDups.size() != list.size()){
			for(Integer i : noDups) {
				if(Collections.frequency(list, i) > mincount - 1) {
					return i * mincount;
				}
			}
		}		
		return 0;
	}
	
	private List<Integer> listWithNoDup(List<Integer> list) {
		List<Integer> list2 = new ArrayList<>();
		list2.addAll(new HashSet<>(list));
		Collections.sort(list2);
		Collections.reverse(list2);
		return list2;
	}

	private List<Integer> convertToList(int[] is) {
		List<Integer> list =  IntStream.of(is).boxed().collect(Collectors.toList());
		Collections.sort(list);
		Collections.reverse(list);
		return list;
	}



}
