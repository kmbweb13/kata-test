var Frame = (function () {
	var clazz = function () {
		this.lastFrame = false;
		this.strike = false;
		this.spare = false;
		this.score = 0;
		this.bonusScore = 0;
		this.roll = [];
	}

	clazz.prototype.init = function (frame) {
		this.roll = frame;
		var currentScore = frame[0];
		this.strike = currentScore === 10;
		var roll2 = frame[1]
		currentScore += roll2;
		
		if(!this.strike && currentScore === 10) {
			this.spare = true;
		}
		if(frame.length === 3){
			this.bonusScore += roll2 + frame[2];
			this.lastFrame = true;
		}

		this.score = this.strike ? 10 : currentScore;
	}

	clazz.prototype.calculateScore = function (frames) {
		if (this.strike) {
			var len = frames.length;
			if(!this.lastFrame && len > 0){
				var frame = frames[0];
				this.bonusScore += frame.score;
				if(frame.strike) {
					switch (len) {
						case 1:
							this.bonusScore += frames[0].roll[1];
							break;
						case 2:
							this.bonusScore += frames[1].score;
							break;
					}
				}				
			}
		}
	}

	clazz.prototype.totalScore = function () {
		return this.score + this.bonusScore;
	}

	return clazz;
})();