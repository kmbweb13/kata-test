var Coin = (function() {

	var clazz = function (weight, diameter, thickness)  {

		if(!(this instanceof Coin)) {
			return new Coin(weight, diameter, thickness);
		}
		this.weight = weight;
		this.diameter = diameter;
		this.thickness = thickness;
		this.name = "";
		this.value = "";

		if (weight === 2.5 && diameter === 19.05 && thickness === 1.52) {
			this.name = "penny";
			this.value = 0.01;
		}
		else if (weight === 5.0 && diameter === 21.21 && thickness === 1.95) {
			this.name = "nickel";
			this.value = 0.05;
		}
		else if (weight === 2.268 && diameter === 17.91 && thickness === 1.35) {
			this.name = "dime";
			this.value = 0.1;
		}
		else if (weight === 5.67 && diameter === 24.26 && thickness === 1.75) {
			this.name = "quarter";
			this.value = 0.25;
		}
		else {
			this.name = "invalid";
			this.value = .00;
		}
	};

	clazz.prototype.getName = function() {
		return this.name;
	}

	clazz.prototype.getValue = function() {
		return this.value;
	}
	return clazz;
})();

var Product = (function() {
	var clazz = function (name, cost)  {
		if(!(this instanceof Product)) {
			return new Product(name, cost);
		}
		this.name = name;
		this.cost = cost;
	};

	clazz.prototype.getName = function() {
		return this.name;
	}

	clazz.prototype.getCost = function() {
		return this.cost;
	}
	return clazz;
})();


var VendingMachine = (function() {
	var clazz = function (nickels, dimes, quarters, cola, chips, candy)  {
		if(!(this instanceof VendingMachine)) {
			return new VendingMachine(nickels, dimes, quarters, cola, chips, candy);
		}
		this.changeBucket = {
			"nickels": {
				coin: Coin(5.0, 21.21, 1.95),
				qnty:nickels
			},
			"dimes": {
				coin: Coin(2.268, 17.91, 1.35),
				qnty:dimes
			},
			"quarters": {
				coin: Coin(5.67, 24.26, 1.75),
				qnty:quarters
			}
		}
		this.products = {
			"Cola": {
				product: Product("Cola", 1.00),
				qnty:cola
			},
			"Chips": {
				product: Product("Chips", 0.5),
				qnty:chips
			},
			"Candy": {
				product: Product("Candy", .65),
				qnty:candy
			}
		};
		this.displayValue = "";
		this.collectedBucket = 0;
		this.acceptedCustomerCoinsValue = 0;
		this.adjustAcceptedCustomerCoinsValue = function (amount) {
			this.acceptedCustomerCoinsValue += amount;
			this.acceptedCustomerCoinsValue  = Number(this.acceptedCustomerCoinsValue.toFixed(2));
		}
	};

	clazz.prototype.getTotalChangeAvailable = function() {
		var totalChangeAvailable = 0;
		var dimes = this.changeBucket.dimes, nickels = this.changeBucket.nickels, quarters = this.changeBucket.quarters;
		totalChangeAvailable += nickels.qnty * nickels.coin.getValue();
		totalChangeAvailable += dimes.qnty * dimes.coin.getValue();
		totalChangeAvailable += quarters.qnty * quarters.coin.getValue();

		return totalChangeAvailable;
	}

	clazz.prototype.getColaCount = function() {
		return this.products.Cola.qnty;
	}
	clazz.prototype.getChipsCount = function() {
		return this.products.Chips.qnty;
	}
	clazz.prototype.getCandyCount = function() {
		return this.products.Candy.qnty;
	}
	clazz.prototype.getDisplayValue = function() {
		if(this.displayValue.length > 0) {
			var displayValue = this.displayValue;
			this.displayValue =  "";
			return displayValue;
		}

		var value = this.getAcceptedCustomerCoinsValue();
		if(value === 0) {
			return "INSERT COIN";
		}
		return value.toFixed(2);
	}
	clazz.prototype.acceptCoin = function(weight, diameter, thickness) {
		var coin = Coin(weight, diameter, thickness), value = coin.getValue();
		if(value != .01 && value != 0){
			this.adjustAcceptedCustomerCoinsValue(value);
			return true;
		}
		return false;
	}

	clazz.prototype.getAcceptedCustomerCoinsValue = function() {
		return this.acceptedCustomerCoinsValue;
	}

	clazz.prototype.dispenseCandy = function() {
		var candy = this.products.Candy;
		var cost = candy.product.getCost();

		if(this.getCandyCount() > 0 && this.getAcceptedCustomerCoinsValue() >= cost) {
			this.adjustAcceptedCustomerCoinsValue(-cost);
			candy.qnty -= 1;
			this.displayValue = "THANK YOU";
			return true;
		}
		this.displayValue = cost.toFixed(2)
		return false;
	}

	clazz.prototype.getCandyCount = function() {
		return this.products.Candy.qnty;
	}

	clazz.prototype.dispenseCola = function() {
		var cola = this.products.Cola;
		var cost = cola.product.getCost();

		if(this.getColaCount() > 0 && this.getAcceptedCustomerCoinsValue() >= cost) {
			this.adjustAcceptedCustomerCoinsValue(-cost);
			cola.qnty -= 1;
			this.displayValue = "THANK YOU";
			return true;
		}
		this.displayValue = cost.toFixed(2)
		return false;
	}

	clazz.prototype.getColaCount = function() {
		return this.products.Cola.qnty;
	}

	clazz.prototype.dispenseChips = function() {
		var chips = this.products.Chips;
		var cost = chips.product.getCost();

		if(this.getChipsCount() > 0 && this.getAcceptedCustomerCoinsValue() >= cost) {
			this.adjustAcceptedCustomerCoinsValue(-cost);
			chips.qnty -= 1;
			this.displayValue = "THANK YOU";
			return true;
		}
		this.displayValue = cost.toFixed(2)
		return false;
	}

	clazz.prototype.processProduct = function (product) {		
		var cost = product.product.getCost();

		if(this.getChipsCount() > 0 && this.getAcceptedCustomerCoinsValue() >= cost) {
			this.adjustAcceptedCustomerCoinsValue(-cost);
			product.qnty -= 1;
			this.displayValue = "THANK YOU";
			return true;
		}
		this.displayValue = cost.toFixed(2)
		return false;
	}

	clazz.prototype.getChipsCount = function() {
		return this.products.Chips.qnty;
	}
	clazz.prototype.refundExtraAcceptedAmount = function() {
		if(this.acceptedCustomerCoinsValue > 0) {
			var returnValue = this.getAcceptedCustomerCoinsValue();
			this.acceptedCustomerCoinsValue = 0;
			return returnValue;
		}
		return 0;
	}
	return clazz;
})();