describe("VendingMachine", function() {

	describe("Coin Validaion", function() {


		it("penny", function() {
			var coin = Coin(2.5, 19.05, 1.52);
			expect(coin.getName()).toEqual("penny");
			expect(coin.getValue()).toEqual(0.01);
		});
		it("nickel", function() {
			var coin = Coin(5.0, 21.21, 1.95);
			expect(coin.getName()).toEqual("nickel");
			expect(coin.getValue()).toEqual(0.05);
		});
		it("dime", function() {
			var coin = Coin(2.268, 17.91, 1.35);
			expect(coin.getName()).toEqual("dime");
			expect(coin.getValue()).toEqual(0.10);
		});
		it("quarter", function() {
			var coin = Coin(5.67, 24.26, 1.75);
			expect(coin.getName()).toEqual("quarter");
			expect(coin.getValue()).toEqual(0.25);
		});
		it("invalid", function() {
			var coin = Coin(3.0, 19.05, 1.52);
			expect(coin.getName()).toEqual("invalid");
			expect(coin.getValue()).toEqual(0.00);
		});
	});

	describe("Product Validaion", function() {
		it("Product", function() {
			var product = Product("Cola", 1.00);
			expect(product.getName()).toEqual("Cola");
			expect(product.getCost()).toEqual(1.00);
		});
	});

	describe("Vending Machine Validaion", function() {
		var vendingMachine;

		beforeEach(function() {
			vendingMachine = VendingMachine(10, 10, 10, 10, 10, 10);
		});

		it("Vending Machine initalize", function() {
			expect(vendingMachine.getTotalChangeAvailable()).toEqual(4.00);
			expect(vendingMachine.getColaCount()).toEqual(10);
			expect(vendingMachine.getChipsCount()).toEqual(10);
			expect(vendingMachine.getCandyCount()).toEqual(10);
			expect(vendingMachine.getDisplayValue()).toEqual("INSERT COIN");
		});

		it("Reject invalid coin", function() {
			expect(vendingMachine.getDisplayValue()).toEqual("INSERT COIN");
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(0);
			expect(vendingMachine.acceptCoin(2.0, 19.5, 2.1)).toEqual(false);
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(0);
			expect(vendingMachine.getDisplayValue()).toEqual("INSERT COIN");
		});


		it("reject penny", function() {
			expect(vendingMachine.getDisplayValue()).toEqual("INSERT COIN");
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(0);
			expect(vendingMachine.acceptCoin(2.5, 19.05, 1.52)).toEqual(false);
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(0);
			expect(vendingMachine.getDisplayValue()).toEqual("INSERT COIN");
		});

		it("Accept nickel", function() {
			expect(vendingMachine.getDisplayValue()).toEqual("INSERT COIN");
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(0);
			expect(vendingMachine.acceptCoin(5.0, 21.21, 1.95)).toEqual(true);
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(.05);
			expect(vendingMachine.getDisplayValue()).toEqual("0.05");
		});
		it("Accept dime", function() {
			expect(vendingMachine.getDisplayValue()).toEqual("INSERT COIN");
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(0);
			expect(vendingMachine.acceptCoin(2.268, 17.91, 1.35)).toEqual(true);
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(.1);
			expect(vendingMachine.getDisplayValue()).toEqual("0.10");
		});
		it("Accept quarter", function() {
			expect(vendingMachine.getDisplayValue()).toEqual("INSERT COIN");
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(0);
			expect(vendingMachine.acceptCoin(5.67, 24.26, 1.75)).toEqual(true);
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(.25);
			expect(vendingMachine.getDisplayValue()).toEqual("0.25");
		});
		it("Accept Multiple Coins", function() {
			expect(vendingMachine.getDisplayValue()).toEqual("INSERT COIN");
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(0);
			expect(vendingMachine.acceptCoin(5.67, 24.26, 1.75)).toEqual(true);
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(.25);
			expect(vendingMachine.acceptCoin(2.268, 17.91, 1.35)).toEqual(true);
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(.35);
			expect(vendingMachine.acceptCoin(2.5, 19.05, 1.52)).toEqual(false);
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(.35);
			expect(vendingMachine.acceptCoin(5.67, 24.26, 1.75)).toEqual(true);
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(.60);
			expect(vendingMachine.acceptCoin(5.0, 21.21, 1.95)).toEqual(true);
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(.65);
			expect(vendingMachine.getDisplayValue()).toEqual("0.65");
		});

		it("Dispense candy", function() {

			expect(vendingMachine.getDisplayValue()).toEqual("INSERT COIN");
			vendingMachine.acceptCoin(5.67, 24.26, 1.75);
			vendingMachine.acceptCoin(2.268, 17.91, 1.35);
			vendingMachine.acceptCoin(5.67, 24.26, 1.75);
			vendingMachine.acceptCoin(5.0, 21.21, 1.95);

			expect(vendingMachine.getDisplayValue()).toEqual("0.65");
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(.65);
			expect(vendingMachine.dispenseCandy()).toEqual(true);
			expect(vendingMachine.getDisplayValue()).toEqual("THANK YOU");
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(0);
			expect(vendingMachine.getCandyCount()).toEqual(9);
			expect(vendingMachine.getDisplayValue()).toEqual("INSERT COIN");

		});

		it("Dispense cola with refund", function() {

			expect(vendingMachine.getDisplayValue()).toEqual("INSERT COIN");
			vendingMachine.acceptCoin(5.67, 24.26, 1.75);
			vendingMachine.acceptCoin(5.67, 24.26, 1.75);
			vendingMachine.acceptCoin(5.67, 24.26, 1.75);
			vendingMachine.acceptCoin(5.67, 24.26, 1.75);
			vendingMachine.acceptCoin(5.0, 21.21, 1.95);

			expect(vendingMachine.getDisplayValue()).toEqual("1.05");
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(1.05);
			expect(vendingMachine.dispenseCola()).toEqual(true);			
			expect(vendingMachine.getDisplayValue()).toEqual("THANK YOU");
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(.05);
			expect(vendingMachine.getColaCount()).toEqual(9);
			expect(vendingMachine.getDisplayValue()).toEqual("0.05");
			expect(vendingMachine.refundExtraAcceptedAmount()).toEqual(.05);
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(0);
			expect(vendingMachine.getDisplayValue()).toEqual("INSERT COIN");
		});

		it("Dispense chips", function() {

			expect(vendingMachine.getDisplayValue()).toEqual("INSERT COIN");
			vendingMachine.acceptCoin(5.67, 24.26, 1.75);
			vendingMachine.acceptCoin(5.67, 24.26, 1.75);

			expect(vendingMachine.getDisplayValue()).toEqual("0.50");
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(.5);
			expect(vendingMachine.dispenseChips()).toEqual(true);		
			expect(vendingMachine.getDisplayValue()).toEqual("THANK YOU");
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(0);
			expect(vendingMachine.getChipsCount()).toEqual(9);
			expect(vendingMachine.getDisplayValue()).toEqual("INSERT COIN");
		});

		it("Don't dispense chips - coin value low.", function() {

			expect(vendingMachine.getDisplayValue()).toEqual("INSERT COIN");
			vendingMachine.acceptCoin(5.67, 24.26, 1.75);
			vendingMachine.acceptCoin(5.0, 21.21, 1.95);

			expect(vendingMachine.getDisplayValue()).toEqual("0.30");
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(.3);
			expect(vendingMachine.dispenseChips()).toEqual(false);
			expect(vendingMachine.getDisplayValue()).toEqual("0.50");
			expect(vendingMachine.getAcceptedCustomerCoinsValue()).toEqual(.3);
			expect(vendingMachine.getChipsCount()).toEqual(10);
			expect(vendingMachine.getDisplayValue()).toEqual("0.30");
		});


	});
});