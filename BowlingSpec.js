describe("Frame", function() {
  var frame;

  beforeEach(function() {
    frame = new Frame();
  });

  it("Init frame with less then 10 combined total  strike and spare sould be false", function () {
  	var roll = [0,0];
  	frame.init(roll);
  	expect(frame.strike).toBe(false);
  	expect(frame.spare).toBe(false);
  	expect(frame.score).toEqual(0);
  	expect(frame.roll).toEqual(roll);
  });

  it("Init frame with 10 on first roll strike sould be true", function () {
  	frame.init([10,0]);
  	expect(frame.strike).toBe(true);
  	expect(frame.spare).toBe(false);
  	expect(frame.score).toEqual(10);
  });

  it("Init frame with 10 combined total strike should be false and spare sould be true", function () {
  	frame.init([8,2]);
  	expect(frame.strike).toBe(false);
  	expect(frame.spare).toBe(true);
  	expect(frame.lastFrame).toBe(false);
  	expect(frame.score).toEqual(10);
  });


  it("Init frame with 3 strikes score should be 30 ", function () {
  	frame.init([10,10,10]);
  	expect(frame.score).toEqual(10);
  	expect(frame.totalScore()).toEqual(30);
  	expect(frame.lastFrame).toBe(true);
  });

  it("process 3 frames with all gutters score should be 0 ", function () {
  	
  	frame.init([0,0]);
  	var frame2 = new Frame(), frame3 = new Frame();
  	frame2.init([0,0]);
  	frame3.init([0,0]);
  	frame.calculateScore([frame2, frame3]);
  	expect(frame.score).toEqual(0);
  });

  it("process 3 frames with all strikes totalscore should be 30 ", function () {
  	
  	frame.init([10,0]);
  	var frame2 = new Frame(), frame3 = new Frame();
  	frame2.init([10,0]);
  	frame3.init([10,0]);
  	frame.calculateScore([frame2, frame3]);
  	expect(frame.score).toEqual(10);
  	expect(frame.totalScore()).toEqual(30);
  });

  it("process 2 frames with all strikes totalscore should be 30 ", function () {
  	
  	frame.init([10,0]);
  	var frame2 = new Frame(), frame3 = new Frame();
  	frame2.init([10,10,10]);
  	frame.calculateScore([frame2]);
  	expect(frame.score).toEqual(10);
  	expect(frame.totalScore()).toEqual(30);
  });


  it("process 2 frames with all strikes totalscore should be 25 ", function () {
  	
  	frame.init([10,0]);
  	var frame2 = new Frame(), frame3 = new Frame();
  	frame2.init([10,5,10]);
  	frame.calculateScore([frame2]);
  	expect(frame.score).toEqual(10);
  	expect(frame.totalScore()).toEqual(25);
  });
  it("process 2 frames with all strikes totalscore should be 25 ", function () {
  	
  	frame.init([10,0]);
  	var frame2 = new Frame(), frame3 = new Frame();
  	frame2.init([2,5]);
  	frame.calculateScore([frame2]);
  	expect(frame.score).toEqual(10);
  	expect(frame.totalScore()).toEqual(17);
  });

  // it("Init frame with 0 on first roll and 0 on second strike sould be false", function () {
  // 	frame.init([0,0]);
  // 	expect(frame.strike).toBe(false);
  // });
  // it("Init frame with 5 on first roll and 5 on second spare sould be false", function () {
  // 	frame.init([5,5]);
  // 	expect(frame.spare).toBe(true);
  // });
});
describe("Bowling", function() {
  var game;

 //  beforeEach(function() {
 //    game = new Bowling();
 //  });

 //  it("Rolling all gutters should have a score of 0", function () {
 //  	game.processGame([[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]]);
 //  	expect(game.finalScore).toEqual(0);
 //  });

 //  it("Rolling all strikes should have a score of 300", function () {
 //  	game.processGame([[10, 0], [10, 0], [10, 0], [10, 0], [10, 0], [10, 0], [10, 0], [10, 0], [10, 0], [10, 10, 10]]);
	// expect(game.finalScore).toEqual(300);
 //  });


 //  it("Rolling 5 on first and gutter on second should have a score of 50", function () {
 //  	var frames = [];
 //  	for (var i = 0; i < 10 ; i++) {
 //  		frames.push([5,0]);
 //  	}
 //  	game.processGame(frames);
	// expect(game.finalScore).toEqual(50);
 //  });

 //  it("Rolling all 5's should have a score of 150", function () {
 //  	var frames = [];
 //  	for (var i = 0; i < 10 ; i++) {
 //  		if(i + 1 === 10) {
	// 		frames.push([5,5,5]);
  		
 //  		} else {
 //  			frames.push([5,5]);
 //  		}
 //  	}
 //  	game.processGame(frames);
	// expect(game.finalScore).toEqual(150);
 //  });


 //  it("Rolling all strikes except gutter on the last roll should have a score of 290", function () {
 //  	game.processGame([[10, 0], [10, 0], [10, 0], [10, 0], [10, 0], [10, 0], [10, 0], [10, 0], [10, 0], [10, 10, 0]]);
	// expect(game.finalScore).toEqual(290);
 //  });


 //  it("Rolling strike then all 4's on next frame should have a score of 122", function () {
 //  	var frames = [];
 //  	for (var i = 0; i < 10 ; i++) {
 //  		if(i % 2 === 0)
 //  		{
	// 		frames.push([4,4]);
 //  		}
 //  		else {
 //  			frames.push([10,0]);
 //  		}
  		
 //  	}
 //  	game.processGame(frames);
	// expect(game.finalScore).toEqual(122);
 //  });

});
